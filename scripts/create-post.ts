import fs from 'fs-extra'
import path from 'path'
import { nanoid } from 'nanoid'
import yargs from 'yargs/yargs'
import chalk from 'chalk'

const getGetterValue = <T extends ((...args: any[]) => any) | any>(getter: T): T extends (...args: any[]) => infer R ? R : T => {
  return typeof getter === 'function' ? getter() : getter
}

const getContent = (props: Record<string, string>) =>
`---
date: ${props.date}
---`

;(async () => {
  const argv = yargs(process.argv.slice(2))
    .option('date', {
      type: 'string',
      default: () => {
        const now = new Date()
        return `${now.getFullYear()}-${now.getMonth() + 1}-${now.getDate()}`
      },
    })
    .option('name', {
      type: 'string',
      default: () => {
        return nanoid(10)
      },
    })
    .option('store', {
      type: 'string',
      default: () => {
        return path.resolve(__dirname, '../src/_posts')
      },
    })
    .option('dir', {
      type: 'boolean',
      default: true,
    })
    .argv

  const store = getGetterValue(argv.store)
  const name = getGetterValue(argv.name)
  const date = getGetterValue(argv.date)
  const isDir = getGetterValue(argv.dir)

  if (await fs.pathExists(store) === false) {
    console.log(chalk.red(`${store} 不存在`))
    process.exit(1)
  }

  const identify = [date, name].join('-')
  const filename = isDir ? path.join(identify, 'README.md') : `${identify}.md`
  const filepath = path.join(store, filename)

  if (await fs.pathExists(filepath) === true) {
    console.log(chalk.red(`檔案 ${filename} 已經存在，請更換名稱`))
    process.exit(1)
  }

  console.log(`${chalk.green(`建立 ${filename}`)} ${chalk.gray(`(${store})`)}`)

  await fs.createFile(filepath)

  await fs.writeFile(filepath, getContent({
    date,
  }))
})()