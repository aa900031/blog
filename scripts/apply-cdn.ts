import path from 'path';
import glob from 'fast-glob';
import fs from 'fs-extra';

const noneStaticallyImageRegex = [
  /<img.*?src=['"](https?:\/\/(?!cdn\.statically\.io)(\S+))['"].*?>/g, // <img src="*">
  /!\[[^\]]*\]\((https?:\/\/(?!cdn\.statically\.io)(\S+))\)/g, // ![](*)
]

;(async () => {
  const PATH_POSTS_DIR = path.resolve(__dirname, '../src/_posts')

  const mdFiles = await glob([
    path.join(PATH_POSTS_DIR, '**/*.md'),
  ], { absolute: true })

  for (const filePath of mdFiles) {
    const fileContent = (await fs.readFile(filePath)).toString()
    let nextContent = fileContent
    let result: RegExpExecArray
    let isReplaced = false

    for (const regex of noneStaticallyImageRegex) {
      while ((result = regex.exec(fileContent)) !== null) {
        isReplaced = true
        const [all, url, path] = result
        const nextAll = all.replace(url, `https://cdn.statically.io/img/${path}`)
        nextContent = nextContent.replace(all, nextAll)
      }
    }

    if (isReplaced) {
      console.log(`替換 ${filePath} 內的圖片至 "cdn.statically.io"`);
      await fs.writeFile(filePath, nextContent)
    }
  }
})()
