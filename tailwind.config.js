module.exports = {
  purge: [
    './src/.vuepress/**/*.{vue,js,ts,jsx,tsx}',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        'vuepress-accent': 'var(--vuepress-color-accent)',
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
