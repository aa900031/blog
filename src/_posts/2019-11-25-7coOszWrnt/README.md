---
date: 2019-11-25
tags:
  - Notion
  - Cloudflare Worker
---

# Notion 自訂網址

目前的 Notion 沒有開放自定義網址((至少我現在的方案沒有，好在發現某位[大大的 gits](b9fefc9625b76f70488e5d8c2a99315d)，用 Cloudflare Worker 轉發到 Notion。
這邊的程式是我額外改的，可一次託管多個 Domain 至 Notion

[Notion custom domain ($1919066) · Snippets](https://gitlab.com/snippets/1919066)

主程式片段

## 將 DNS 託管至 Cloudflare

使用 Cloudflare worker 需要把整個 domain 交給 Cloudflare 託管，可以參考官方說明文件

[Changing your domain nameservers to Cloudflare](https://support.cloudflare.com/hc/en-us/articles/205195708-Changing-your-domain-nameservers-to-Cloudflare)

## Cloudflare Worker 腳本

1. 進入 [Cloudflare Dashboard](https://dash.cloudflare.com/)
2. 點擊 Worker Tab
3. 點選 Launch Editor 按鈕
4. 點選 Add script 按鈕 & 命名
5. 將[程式碼](https://gitlab.com/snippets/1919066)貼上
6. 改寫 `YOUR_DOMAIN` 成你的 Domain ex: `example.io`
7. 改寫 `YOUR_NOTION_PAGE_ID` 成你預設的 Notion Page ID
8. 點擊 Deploy 按鈕
9. 選擇 route 至你的 Domain ex: `example.io/*`
10. 大功告成

## 參考文件

- [Use a custom domain for a public Notion page](https://notion-tricks.com/Use-a-custom-domain-for-a-public-Notion-page-c65c2475c520432e8bd87195edf7c464)
- [notion.so custom domain](https://gist.github.com/mayneyao/b9fefc9625b76f70488e5d8c2a99315d)
