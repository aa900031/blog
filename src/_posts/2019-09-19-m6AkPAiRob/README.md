---
date: 2019-09-19
tags:
  - Laravel
  - PHP
  - Docker
  - macOS
---

# 輕鬆玩 Laravel 環境篇

> 安裝PHP環境好麻煩！ 有好多教學步驟好多看不懂！ 環境怎麼一直架設不起來！好失望

這些大多都是我自己在學習Laravel時心裡的OS，抑或是現在也一樣，在新的電腦上要安裝環境總是會遇到一堆問題，什麼東西沒裝要先裝才能編譯，或是平台不同/版本不同要有不一樣的解決方式，看到這裡就心涼一半。

## 安裝 Homebrew

首先要先安裝macOS上面的套件管理利器 [Homebrew](https://brew.sh/)

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

等他跑完就安裝好了～

## 安裝 Composer

PHP的套件管理 [Composer](https://getcomposer.org/)

```bash
brew install composer
```

安裝完後再用下面指令測試

```bash
composer –version
```

## 安裝 Laravel Installer

這就是Laravel專案的建構器，用這個就可以一個指令建立好空的專案

```bash
composer global require "laravel/installer"
```

## 新建專案

`my-project` 這邊換成你的專案名即可

```bash
laravel new "my-project"
```

## 安裝 Docker

- 先更新 brew 資訊，再來安裝 docker brew update brew cask install docker
- 進到 `應用程式` 資料夾 打開 `Docker.app`
- 按 `OK` 後輸入管理者密碼 ## 整合 Laradock

這次教學的重頭戲 [laradock](http://laradock.io/) 這是一個 Docker 的 Laravel 開發環境，讓你輕鬆用 Docker 就可以建立起環境，在基礎情況下可以零設定架好

- 進入剛才建立好的專案 ex: `my-project` cd ./my-project
- 用 `git clone` 把 laradock 拷貝下來 git clone https://github.com/Laradock/laradock.git

此時你的專案結構會是這樣

```
my-project
    |-- laradock
```

- 進入 laradock 資料夾並重新命名 `env-example` 成 `.env` cd ./laradock cp env-example .env
- 啟動容器 docker-compose up -d nginx mysql phpmyadmin redis workspace
- 編輯 laravel 專案的 `.env` 檔 DB_HOST=mysql REDIS_HOST=*redis*
- 開啟 `http://localhost`

到這裡基本上的東西都完成了👏👏👏
