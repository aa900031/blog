---
date: 2020-07-13
tags:
  - Kubernetes
---

# k9s 破圖
![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-49-36-20210316014936.png)

今天早上用 k9s 有點問題，破圖的很嚴重最後找到這篇 [issue](https://github.com/derailed/k9s/issues/448)，解法如下：

編輯 .bashrc (或是你用 .zshrc) 改寫 k9s 指令，加上 `LC_CTYPE` 並指定成 en_US.UTF-8

```bash
alias k9s="LC_CTYPE='en_US.UTF-8' k9s"
```
