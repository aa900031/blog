---
date: 2019-12-02
tags:
  - Vue
  - VSCode
  - Virtual Studio Code
  - Vetur
---
# Vetur Typescript Snippets

## 環境

- [VSCode](https://code.visualstudio.com/)
- [Vetur](https://github.com/vuejs/vetur)

## 下載 Snippets

這邊做了 Vue 兩個 Component 型態分別為：Component, Functional Component

[typescript-functional.vue](https://gitlab.com/snippets/1919062)

[typescript.vue](https://gitlab.com/snippets/1919064)


## 放入 Vetur Snippet

以下謹紀錄，實際請以官方教學為主

- 開啟 VSCode
- 開啟 指令區 (Command + Shift + P)
- 搜尋 `Vetur: Open user scaffold snippet folder` & 按下 `Eneter`
- VSCode 將會開啟其他編輯視窗
- 新增 Snippets 至 `script` 資料夾內

[可參考官方文件](https://vuejs.github.io/vetur/snippet.html#customizable-scaffold-snippets)
