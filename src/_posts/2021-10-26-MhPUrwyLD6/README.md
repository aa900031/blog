---
date: 2021-10-26
tags:
  - iTerm
  - macOS
  - Terminal
---

# iTerm 用滑鼠滾動更新 Vim 游標

1. 打開 iTerm 並開啟偏好設定
2. 點擊 "Advanced" 標籤
3. 找到 `Scroll wheel sends arrow keys when in alternate screen mode`
4. 把值改成 `Yes`

也可以參考以下圖片
![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/10/26-20-52-47-20211026205247.png)
