---
date: 2020-02-19
tags:
  - Docker
  - Traefik
  - Go Lang
  - Github
---
# Traefik config Template 用法

::: tip
2020/03/03 更新：官方文件已有 Go Template, sprig 相關解說～
:::

## Traefik 是什麼？

是一套提供反向代理與負載平衡的服務器，有點像是 Nginx 或 HAProxy，優點大致上就是速度快、設定簡單、深度結合 docker、內建 Let's Encrypt 服務

這邊就簡單帶過，主要是紀錄查詢 Traefik 的 Template 的坑

## Traefik config

官方提供很多設定方式，有一種就是 `File` 主要是寫 yaml, toml 等設定檔

## Traefik config Template

[官方文件](https://docs.traefik.io/providers/file/#go-templating)有寫到可以用 Go Template 寫設定，讓設定可程式化，但完全沒有寫語法，僅僅只有一個用不太到的 range 產生多個 routes, services, tls

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-50-32-20210316015032.png)

## Go Template

由官方文件可得知 Template 解析器是 Go 官方套件

[template - The Go Programming Language](https://golang.org/pkg/text/template/)

進來後找到一些邏輯判斷、變數、內建函數等等的語法，但由於我想用 env 取得設定，來產生 config，目前還是不符合我的需求

## 查原始碼

終於走到這一步了！文件寫得差就只能找程式來看QQ，簡單來說直接查詢 `text/template` 即可定位到下述程式：

[containous/traefik](https://github.com/containous/traefik/blob/master/pkg/provider/file/file.go)

Provide → BuildConfiguration → loadFileConfig → CreateConfiguration
大致上可以看得出這邊在讀取檔案，然後丟進去 template 處理

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-50-53-20210316015053.png)

其中 `defaultFuncMap` 讓我很好奇，一開始這變數是由 `sprig.TxtFuncMap()` 產生，這個 `sprig` 是另外一個第三方套件，專門提供好用的 template 函式

[Masterminds/sprig](https://github.com/Masterminds/sprig)

然而裡面就有提供 `env` 讀取環境變數做設定！

## 總結

要寫 Traefik config 還真的不簡單，不僅要看 Traefik 官方文件還要把 Go Template 文件看完，更需要查 Traefik 原始碼看他裡面的實作，才得知 Sprig

[Go template 官方文件](https://golang.org/pkg/text/template/)

[Sprig 官方文件](http://masterminds.github.io/sprig/)
