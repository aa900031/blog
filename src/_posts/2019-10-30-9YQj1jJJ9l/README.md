---
date: 2019-10-30
tags:
  - Docker
  - Kubernetes
  - macOS
---
# Kubernetes for Mac

## 安裝 Kubernetes

首先你要有安裝 [Docker for Mac](https://docs.docker.com/docker-for-mac/)

1. Docker for Mac 選單右鍵
2. Preferences/Kubernetes Tab
3. Enable Kubernetes 打勾
4. Apply

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-51-43-20210316015143.png)

## 安裝 Helm

Helm 是 Kubernetes 的安裝器，想像成 k8s 的 npm 就可以了，用了這個之後一鍵部署 APP 不是夢想。

使用 brew 安裝 helm

```bash
brew install helm
```

安裝 repo

```jsx
helm repo add stable https://kubernetes-charts.storage.googleapis.com/
```

列出 stable repo

```jsx
helm search repo stable
```

## Ingress 安裝

官方推薦使用 [ingress-nginx](https://kubernetes.github.io/ingress-nginx/) 作為本地端的 Controller

使用 helm 安裝

```bash
helm install --name nginx-ingress --namespace kube-system stable/nginx-ingress
```
