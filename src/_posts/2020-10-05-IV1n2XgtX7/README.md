---
date: 2020-10-05
tags:
  - eMask
  - 口罩
  - iOS
  - 捷徑
---
# eMask 自動提醒取貨

每次都收到簡訊後，手動加入到 Things 待辦清單裡，最近懶癌發作，索性就寫了一個 iOS 的捷徑代替我完成這任務

::: tip
2021/08/19 修復版 v1.0.1
:::

## 步驟零：下載解析訊息捷徑

[點我下載捷徑(v1.0.1)](https://www.icloud.com/shortcuts/5460ec8d9f3b49be97c81790fdff25c8)

[點我看程式碼](https://gitlab.com/aa900031/shortcut-emask)


## 步驟一：建立自動化操作

1. 開啟 "捷徑" APP
2. 切換到 "自動化" 頁面
3. 點擊 右上角 + 號
4. 點擊 "製作個人自動化操作" (圖左)
5. 選擇 "訊息" (圖右)

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-18-34-20210316011834.jpeg)

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-20-27-20210316012027.jpeg)

## 步驟二：建立訊息規則

1. 點選 訊息包含 右邊的 "選擇"
2. 輸入以下文字

    ```
    您已完成eMask口罩付款
    ```

3. 點擊完成
4. 點擊右上角的 "下一步"

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-20-44-20210316012044.jpeg)

## 步驟三：新增動作

1. 增加 "執行捷徑" 並選擇 "解析 eMask 簡訊"
    1. 輸入的欄位改成 "捷徑輸入"
2. 增加 "從輸入項目取得辭典"
3. 增加 "添加待辦事項"
    1. 何時欄位： `startAt`
    2. 完成時限： `endAt`
    3. 備註： `code`

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-21-2-20210316012102.jpeg)

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-21-15-20210316012115.png)

## 步驟四：檢視指令流程

確認相關內容圖，即可按下 "完成"

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/03/16-1-21-43-20210316012143.png)

## 寫在最後

主要提供 "解析 eMask 簡訊" 的相關資訊，流程部分也只是個範例，最後添加待辦清單的地方也可以不用是 Things，可以使用 iOS 內建的提醒事項 (Reminder)
