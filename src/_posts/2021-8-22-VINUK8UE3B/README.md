---
date: 2021-8-22
tags:
  - VPN
  - docker
  - Surge
  - ProtonVPN
---

# 用 Docker 把 VPN 轉成 Shadowsocks 代理

最近買了一些 VPN 服務來試用 (Windscribe, ProtonVPN)

使用官方的 APP 進行連線，馬上就能夠使用！全部網路都走 VPN 連線

對一般用戶來說很簡單很方便，但對於進階玩家來可以有更好的玩法

<br>

用 Docker 將 VPN 連線轉換成 Shadowsocks 後就可以用 [Surge](https://nssurge.com/) 或 [ShadowsocksClient](https://github.com/shadowsocks/shadowsocks-windows) + [SwitchyOmega](https://github.com/FelisCatus/SwitchyOmega) 做更進階的使用

ex: 玩馬娘連線到日本、看 Netflix 時用連線到美國，其他上網就用自己的網路

中間還不需要中斷連線並重連

<br>

爬文時發現 [Gluetun VPN Client](https://github.com/qdm12/gluetun)
> 用 Go 寫的 VPN Client 內建多組 VPN 供應商，還支援 DNS over TLS、Shadowsocks、Http Proxy

完全符合我的需求，而且還用 Shadowsocks 比起 SOCKS 更安全！






## 取得 Windscribe OpenVPN 帳號密碼

### 登入
[https://windscribe.com/login](https://windscribe.com/login)

### 進入 OpenVPN 下載頁
[https://windscribe.com/getconfig/openvpn](https://windscribe.com/getconfig/openvpn)

### 點 Step 2 "Get Credentials" 按鈕

直接點就好！不需要經過 Step 1

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2021/08/22-16-15-8-20210822161508.png)







## docker run 一鍵啟動

執行以下指令，其中描述請自行更換

- `OPENVPN_USER`, `OPENVPN_PASSWORD` 填入上一步驟拿到的帳密
- `SERVER_HOSTNAME` 可以參考 [gluetun Wiki](https://github.com/qdm12/gluetun/wiki/Windscribe-Servers) 裡面有伺服系列表可供挑選
- `SHADOWSOCKS_PASSWORD` 自定義 Shadowsocks 連線的密碼
- `6000` 是 Shadowsocks 在本機的 port

```sh
docker run -d \
--name gluetun-jp \
--cap-add=NET_ADMIN \
-e VPNSP="windscribe" \
-e OPENVPN_USER="aabbcc" \
-e OPENVPN_PASSWORD="aabbcc" \
-e SHADOWSOCKS=on \
-e SHADOWSOCKS_PASSWORD="1234" \
-e SERVER_HOSTNAME="jp-004.whiskergalaxy.com" \
-p 6000:8388
qmcgaw/gluetun
```

## 進階使用 docker compose

若是要一次開兩個以上的 gluetun 的話建議可使用 docker-compose 統一管理

以下就以需要建立連線到日本、美國，來做範例


### 建立專案資料夾

在你喜歡的地方建立一個資料夾，用來存放相關檔案
ex: `proxy-vpn`


### 建立 .env 檔

在資料夾內(`proxy-vpn`)建立 .env，並填入以下內容

```sh
WINDSCRIBE_USERNAME=aabbcc
WINDSCRIBE_PASSWORD=aabbcc
SHADOWSOCKS_PASSWORD=1234
VPN_JP_SERVER_HOSTNAME=jp-004.whiskergalaxy.com
VPN_JP_SHADOWSOCKS_PORT=6001
VPN_US_SERVER_HOSTNAME=us-west-061.whiskergalaxy.com
VPN_US_SHADOWSOCKS_PORT=6002
```
- `WINDSCRIBE_USERNAME` Windscribe 提供的 OpenVPN 帳號
- `WINDSCRIBE_PASSWORD` Windscribe 提供的 OpenVPN 密碼
- `SHADOWSOCKS_PASSWORD` 自定義 Shadowsocks 連線的密碼
- `VPN_JP_SERVER_HOSTNAME` 連線到日本的伺服器名稱 ([gluetun Wiki](https://github.com/qdm12/gluetun/wiki/Windscribe-Servers))
- `VPN_JP_SHADOWSOCKS_PORT` Shadowsocks 連到日本的 port
- `VPN_US_SERVER_HOSTNAME` 連線到美國的伺服器名稱 ([gluetun Wiki](https://github.com/qdm12/gluetun/wiki/Windscribe-Servers))
- `VPN_US_SHADOWSOCKS_PORT` Shadowsocks 連到美國的 port


### 建立 docker-compose.yml 檔

在資料夾內(`proxy-vpn`)建立 docker-compose.yml ，並填入以下內容

```docker-compose
version: '3.7'

services:
  windscribe-jp:
    image: qmcgaw/gluetun
    restart: unless-stopped
    cap_add:
      - NET_ADMIN
    environment:
      - VPNSP=windscribe
      - OPENVPN_USER=${WINDSCRIBE_USERNAME}
      - OPENVPN_PASSWORD=${WINDSCRIBE_PASSWORD}
      - SERVER_HOSTNAME=${VPN_JP_SERVER_HOSTNAME}
      - SHADOWSOCKS=on
      - SHADOWSOCKS_PASSWORD=${SHADOWSOCKS_PASSWORD}
    ports:
      - ${VPN_JP_SHADOWSOCKS_PORT}:8388
  windscribe-us:
    image: qmcgaw/gluetun
    restart: unless-stopped
    cap_add:
      - NET_ADMIN
    environment:
      - VPNSP=windscribe
      - OPENVPN_USER=${WINDSCRIBE_USERNAME}
      - OPENVPN_PASSWORD=${WINDSCRIBE_PASSWORD}
      - SERVER_HOSTNAME=${VPN_US_SERVER_HOSTNAME}
      - SHADOWSOCKS=on
      - SHADOWSOCKS_PASSWORD=${SHADOWSOCKS_PASSWORD}
    ports:
      - ${VPN_US_SHADOWSOCKS_PORT}:8388

```

### 啟動

在資料夾內(proxy-vpn)執行以下指令

```
docker-compose up -d
```
