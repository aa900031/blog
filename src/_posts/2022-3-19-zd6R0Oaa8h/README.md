---
date: 2022-3-19
tags:
  - 樹莓派
  - Respberry Pi
  - pi
  - Ubuntu
---
# Raspberry Pi 安裝 Ubuntu
## 前言
買了樹莓派第一步驟就要安裝作業系統，這邊就用最簡單的方式紀錄最快速的安裝方式，即使要重灌也很方便

## 什麼是 Cloud Init
這次快速安裝是依賴於 [Cloud Init](https://cloud-init.io/)，這個工具可以使用一個設定檔案就搞定所有的複雜的網路設定、環境設定、安裝軟體...等

## 準備 SD 卡
現在要安裝的是 Ubuntu Server 需要的容量不大，但還是建議有 8G 左右會比較好

## 下載 Raspberry Pi Imager
樹莓派官方推出的系統安裝工具，以下以 macOS 的介面做安裝教學

https://www.raspberrypi.com/software/

進到網頁後下方會有 "Downloads for macOS" 按鈕按下去就對了！

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2022/03/19-20-58-45-20220319205845.png)

## 選擇 Ubuntu 並安裝

點 "CHOOSE OS" 按鈕

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2022/03/19-21-1-57-20220319210157.png)

在彈出視窗中選擇 "Other general-purpose OS"

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2022/03/19-21-3-8-20220319210308.png)

點選 "Ubuntu"

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2022/03/19-21-3-51-20220319210351.png)

選擇 LST 版本的 (目前是 20.04.4)

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2022/03/19-21-4-13-20220319210413.png)

接著會回到原來畫面，接著選擇 Storage 後按下 "Write" 按鈕即可

![](https://cdn.statically.io/img/gitlab.com/aa900031/blog-images/-/raw/master/pictures/2022/03/19-21-4-53-20220319210453.png)

等待安裝...

## 設定 Cloud Init
接著進入這邊教學的重點: Cloud Init 設定

- 純文字編輯器打開 `{你的 SD 卡}/cloud-init/user-data`
- 貼上下方的內容
- `@xxx@` 改成你自己的值

::: tip 提示
`#cloud-config` 請一定要保留！不然會無法使用
:::

```yaml
#cloud-config

# SSH 帳號密碼登入: 禁用 (之後用 SSH Key 就好)
ssh_pwauth: false

groups:
  # 建立自己的使用者群組
  - @使用者群組@

users:
  # 建立自己的使用者
  - name: @使用者名稱@
    gecos: @使用者名稱@
    primary_group: @使用者群組@
    home: /home/@使用者名稱@
    ssh: /bin/bash
    ssh_authorized_keys:
      # 貼上你的 SSH Public Key
      - ssh-rsa @你的 SSH Key@
write_files:
  # 設定自己的使用者可以使用 sudo 指令 & 不用打密碼
  - content: |
        @使用者名稱@    ALL=(ALL) NOPASSWD: ALL
        Defaults:@使用者名稱@    !requiretty
    owner: root:root
    path: /etc/sudoers.d/@使用者名稱@
    permissions: '0444'

hostname: @主機名稱@
manage_etc_hosts: true
# 安裝最新套件
package_upgrade: true
```
