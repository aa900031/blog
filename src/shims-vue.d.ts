import { PageComputed } from 'vuepress-types'

declare module '*.vue' {
  import Vue from 'vue'
  export default Vue
}

declare module 'vue/types/vue' {
  export interface Vue {
    $pagination: {
      length: number
      pages: PageComputed[]
      hasPrev: boolean
      prevLink: string | undefined
      hasNext: boolean
      nextLink: string | undefined
      getSpecificPageLink: (page: number) => string | undefined
    }
    $frontmatterKey: {
      list: {
        name: string;
        path: string;
        pages: PageComputed[];
      }[]
    }
  }
}
