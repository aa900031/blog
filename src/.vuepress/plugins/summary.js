const removeMarkdown = require('remove-markdown');

module.exports = (options) => {
  const ellipsisText = options.ellipsisText === undefined ? '...' : options.ellipsisText
  const textLength = options.textLength === undefined ? 200 : options.textLength
  const syncFrontmatterDescription = options.syncFrontmatterDescription === undefined ? true : options.syncFrontmatterDescription

  return {
    extendPageData: ({ frontmatter, _strippedContent }) => {
      if (!_strippedContent) return

      if (!frontmatter.summary) {
        frontmatter.summary = removeMarkdown(
          _strippedContent
            .trim()
            .replace(/^#+\s+(.*)/, '')
            .slice(0, textLength)
        ) + ' ' + ellipsisText
      }

      if (syncFrontmatterDescription) {
        frontmatter.description = frontmatter.summary
      }
    }
  }
}
