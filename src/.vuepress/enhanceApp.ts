import VueCompositionAPI from '@vue/composition-api';
import { defineEnhanceApp } from './theme/utils/vuepress-helper';

export default defineEnhanceApp(({
  Vue,
}) => {
  Vue.use(VueCompositionAPI)
})
