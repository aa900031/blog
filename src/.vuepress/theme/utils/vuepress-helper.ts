import type { EnhanceApp, Plugin } from 'vuepress-types';

export const defineEnhanceApp = <T extends EnhanceApp>(fn: T): T => fn;

export const definePlugin = <T>(fn: Plugin<T>): Plugin<T> => fn;
