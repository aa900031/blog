const path = require('path')

module.exports = {
  title: '阿中部落格',
  description: '隨手寫寫的部落格',
  themeConfig: {
    nav: [
      {
        text: '文章',
        link: '/',
      },
      {
        text: '標籤',
        link: '/tag',
      },
    ],
  },
  plugins: [
    [require('./plugins/summary')],
    ['vuepress-plugin-typescript'],
    ['@vuepress/blog', {
      directories: [
        {
          id: 'post',
          dirname: '_posts',
          path: '/',
        },
      ],
      frontmatters: [
        {
          id: 'tag',
          keys: ['tags'],
          path: '/tag/',
        },
      ],
      comment: {
        service: 'disqus',
        shortname: 'zhong666-blog',
      },
      sitemap: {
        hostname: 'https://blog.zhong666.me'
      },
    }],
    ['vuepress-plugin-smooth-scroll'],
    ['@vuepress/plugin-medium-zoom'],
    ['@vuepress/plugin-nprogress'],
    ['container', {
      type: 'tip',
      defaultTitle: '提示'
    }],
    ['container', {
      type: 'warning',
      defaultTitle: '注意'
    }],
    ['container', {
      type: 'danger',
      defaultTitle: '警告'
    }],
  ],
  postcss: {
    plugins: [
      require('tailwindcss'),
      require('autoprefixer'),
    ],
  },
}
